Trabalho 2 - Arquitetura e Organização de Computadores II - 2018/1
Nome: Suélen da Costa Ataide

Este trabalho foi feito da seguinte forma:
A entrada foi convertida de hexadecimal para binário para que fosse efetuada a separação de indice, tag e offset. 
A entrada que antes era uma palavra de tamanho oito, foi convertida para uma palavra de 32 bits.
Após isso ele busca esta palavra de 32 bits na tlb, caso não seja encontrada na tlb, então a busca é feita na cache. Caso também não tenha sido encontrada na cache, é porque ocorreu um miss e a palavra tem que ser colocada na cache.
Os ciclos foram a soma dos tempos de acessos que precisaram ser feitos para buscar a palavra.
