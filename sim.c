#include "sim.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

void conversorHexBin(char num[8], int pos);
int conversorHEX(char num);
int testeStringValida(char *string);
char conversorChar(int num);
void setIndice(int tamanho_cache,int tamanho_bloco, int pos);
void setOffset( int tamanho_bloco, int pos);
void setTag(int pos);
struct stats * calculoHitMiss(int qtd, int tamanho_tlb,int tamanho_cache);
char numBin[32];

struct instrucao{
	char numBin[32];
	char word[10];
	char offset[6];
	int numoffset;
	char tag[32];
	int numtag;
	char indice[9];
	int numindice;	
}instr[1024];

struct tlb1{
	char endereco[32];
};
struct cache{ 
	int validade;
	char indice[9];
	char tag[32];
	char dados [32];
	int usado;
}cache[1000];

struct stats * sim(int tamanho_bloco, int tamanho_cache, int entradas_tlb, char * filename, char * stream){
	int i=0, k=0;
	char c = '1';
	char word[10];
	char stream2[300];
//	int Qtdinstr = 0;
	//struct stats *status;
	//struct stats * status;
	//Configurações invalidas
	
	if(filename == NULL && stream == NULL){
		printf("Configuração invalida\n");
		return NULL;
	}
	if(tamanho_bloco != 32 && tamanho_bloco != 64){
		printf("Configuração invalida\n");
		return NULL;
	}
	if(entradas_tlb != 2 && entradas_tlb != 4){
		printf("Configuração invalida\n");
		return NULL;
	}
	//execução
	if(filename == NULL && stream != NULL){ //calcula só a string 
		int cont=0;
		strcpy(stream2, stream);
		for(i=0; stream2[i] != '\0'; i++){
			c = stream2[i];
			//printf(" %c", c);
			if(c =='\n'){
				//testeStringValida(word);
				conversorHexBin(word, cont);
				cont++;	
				k=0;
			}else{
				word[k]= c;
				k++;
			}
		}
	}else{   //calcula o arquivo inteiro
		FILE *arquivo;
		char linha[100];
		int j=0;
		//int pos;
		arquivo = fopen(filename , "rt");
		if (arquivo == NULL){
			printf("Não foi possível abrir o arquivo!");
			return NULL;
		}
		j=0;
		while (!feof(arquivo) && j<1024){ //le o arquivo
			fgets(linha, 12 , arquivo);
			for (i=0; linha[i] != '\0'; i++){
				if(linha[i] != '\n'){
				instr[j].word[i] = linha[i];
				}
			}
			conversorHexBin(instr[j].word, j); 
			setOffset(tamanho_bloco, j);
			setIndice(tamanho_cache, tamanho_bloco, j);
			setTag(j);
			j++;
		}
		//Qtdinstr = j-1; //por algum motivo ainda desconhecido esta copiado a ultima palavra mais de uma vez.
		//status = calculoHitMiss(Qtdinstr, entradas_tlb);
		j=0;
		fclose(arquivo); //fecha o arquivo
	}
	//return status; 
	return NULL;
}
/*
struct stats * calculoHitMiss(int qtd, int tamanho_tlb,int tamanho_cache){
	struct cache *cacheL1;
	struct tlb1 tlb[tamanho_tlb];
	struct stats *status = NULL;
	int i, j;
	//int tempo;
	for(i=0; i<qtd; j++){
		for (j=0; j<tamanho_tlb; j++){
			if((strcmp(instr[i].numBin,tlb[j].endereco)) == 0){ //se as strings forem iguais retorna 0
				//esta na tlb
				status->hits_tlb += 1;
				if(tamanho_tlb == 4){ //dependendo das entradas o tempo é diferente
					status->cycles +=1;
				}
				if(tamanho_tlb == 8){
					status->cycles +=2;
				}
			}else{
				for(i=0; i<qtd; j++){
					for (j=0; j<tamanho_cache; j++){
						if(((strcmp(instr[i].tag , cacheL1[j].tag)) == 0) && ((strcmp(instr[i].numindice,cacheL1[j].indice) == 0))){
							//esta na cache
							status->hits_l1 += 1;
							if(tamanho_cache == 8){
							status->cycles += 1; 
							}
							if(tamanho_cache == 16){
							status->cycles +=  2;
							}
							if(tamanho_tlb == 4){ //dependendo das entradas o tempo é diferente
								status->cycles +=1;
							}
							if(tamanho_tlb == 8){
								status->cycles +=2;
							}
						}else{
							//não está na cache e nem na tlb
							//carrega endereço pra cache
							strcpy(cacheL1[j].tag,instr[i].tag);
							strcpy(cacheL1[j].indice,instr[i].indice);
							strcpy(cacheL1[j].dados,instr[i].numBin);
							cacheL1[i].usado += 1; 
							//fazer a paerte de botar na tlb
						}
					}
				}
			}
		}
		if(i==0){
			status->misses_l1 = 1;
			status->misses_tlb = 1;
			status->hits_l1 = 0;
			status->hits_tlb = 0;
		}
	}
	return status;
}

*/

int testeStringValida(char *string){ //retorna 0 se a string é mal formada, retorna 1 se esta tudo certo
	int i, j, flag=0;
	char caracteresValidos[]= "0123456789ABCDEF";
	if(string[0] != 'R' && string[0] != 'W'){
		return 0;
	}
	if(string[1] != ' '){
		return 0;
	}
	
	for (i=2; i<8; i++){
		for (j=0; j<16; j++){
			if(string[i] == caracteresValidos[j]){
				flag = 1;
			}
		}
		if(flag == 0){
			return 0;
		}
	flag =0;
	}
		return 1;
	}
void conversorHexBin(char num[8], int pos){ //converte um numero de hexa para binario
	char decimais[9]= "123456789";
	char HEXA[6]= "ABCDEF";
	int num1aux=0;
	int num2aux=0;
	int i, k, L;
	int j =0;
	for (i=2; i<10; i++){
		num1aux= conversorHEX(num[i]);
		//printf("%d", num1aux);
		if(num[i] == '0'){
			instr[pos].numBin[j]='0';
			instr[pos].numBin[j+1]='0';
			instr[pos].numBin[j+2]='0';
			instr[pos].numBin[j+3]='0';
			j+=4; //incrementa j em 4 posições pra proxima.
		}
		for(L=0; L<9; L++){
			num2aux= conversorHEX(decimais[L]);
			if(num1aux == num2aux){ //compara se i já ta como um decimal.
				for(k=3; k>=0; k--){
					instr[pos].numBin[j + k] = conversorChar(num1aux % 2);
					num1aux = num1aux/2;
					//printf("%c", numBin[j]);
				}
				j += 4;
			}
		}
		for(L=0; L<7; L++){
			num2aux= conversorHEX(HEXA[L]);
			if(num1aux == num2aux){ //compara se i já ta como um decimal.
				for(k=3; k>=0; k--){
					instr[pos].numBin[j + k] = conversorChar((num1aux % 2));
					num1aux = (num1aux/2);
					//printf("%c", numBin[j]);
					//j++;
				}
				j += 4;
			}
		}
		
		
	}
}
char conversorChar(int num){ //converte para 1 ou 0
	if (num == 0){
		return '0';
		}else{
			return '1';
			}
	}
int conversorHEX(char num){ //converte o char em um inteiro correspondente
	switch (num){
	case '0':
		return 0;
	case '1':
		return 1;
	case '2':
		return 2;
	case '3':
		return 3;
	case '4':
		return 4;
	case '5':
		return 5;
	case '6':
		return 6;
	case '7':
		return 7;
	case '8':
		return 8;
	case '9':
		return 9;
	case 'A':
		return 10;
	case 'B':
		return 11;
	case 'C':
		return 12;
	case  'D':
		return 13;
	case 'E':
		return 14;
	case 'F':
		return 15;
	}
	return 0;
}
void setTag( int pos){
	int i;
	instr[pos].numtag = 32 - (instr[pos].numindice + instr[pos].numoffset);
	for(i=0; i<instr[pos].numtag; i++){
		instr[pos].tag[i] = instr[pos].numBin[i]; //copia a tag pra dentro do campo tag
		//printf("tag %d: %c \n", i, instr[pos].tag[i]);
	}
}
void setOffset( int tamanho_bloco, int pos){
	if(tamanho_bloco == 32){
		instr[pos].offset[0] =instr[pos].numBin[27];
		instr[pos].offset[1] = instr[pos].numBin[28];
		instr[pos].offset[2] = instr[pos].numBin[29];
		instr[pos].offset[3] = instr[pos].numBin[30];
		instr[pos].offset[4] = instr[pos].numBin[31];
		instr[pos].numoffset = 5;
	
	}
	if(tamanho_bloco == 64){
		instr[pos].offset[0] = instr[pos].numBin[26];
		instr[pos].offset[1] = instr[pos].numBin[27];
		instr[pos].offset[2] = instr[pos].numBin[28];
		instr[pos].offset[3] = instr[pos].numBin[29];
		instr[pos].offset[4] = instr[pos].numBin[30];
		instr[pos].offset[5] = instr[pos].numBin[31];
		instr[pos].numoffset = 6;
	}
}
void setIndice(int tamanho_cache,int tamanho_bloco , int pos){
	int indice;
	if(tamanho_cache == 8){
		indice = ((tamanho_cache*1024) / tamanho_bloco)/2;
		if(indice == 64){
			//calcular	= 6
			instr[pos].numindice = 6;
			if(instr[pos].numoffset == 5){
			instr[pos].indice[5] = instr[pos].numBin[26];
			instr[pos].indice[4] = instr[pos].numBin[25];
			instr[pos].indice[3] = instr[pos].numBin[24];
			instr[pos].indice[2] = instr[pos].numBin[23];
			instr[pos].indice[1] = instr[pos].numBin[22];
			instr[pos].indice[0] = instr[pos].numBin[21];
			}else{
			instr[pos].indice[6] = instr[pos].numBin[25];
			instr[pos].indice[5] = instr[pos].numBin[24];
			instr[pos].indice[4] = instr[pos].numBin[23];
			instr[pos].indice[3] = instr[pos].numBin[22];
			instr[pos].indice[2] = instr[pos].numBin[21];
			instr[pos].indice[1] = instr[pos].numBin[20];
			instr[pos].indice[0] = instr[pos].numBin[19];
			}
		}
		if(indice == 128){
			//calcular = 7
			instr[pos].numindice = 7;
			if(instr[pos].numoffset == 5){
			instr[pos].indice[6] = instr[pos].numBin[26];
			instr[pos].indice[5] = instr[pos].numBin[25];
			instr[pos].indice[4] = instr[pos].numBin[24];
			instr[pos].indice[3] = instr[pos].numBin[23];
			instr[pos].indice[2] = instr[pos].numBin[22];
			instr[pos].indice[1] = instr[pos].numBin[21];
			instr[pos].indice[0] = instr[pos].numBin[20];
			}else{
			instr[pos].indice[6] = instr[pos].numBin[25];
			instr[pos].indice[5] = instr[pos].numBin[24];
			instr[pos].indice[4] = instr[pos].numBin[23];
			instr[pos].indice[3] = instr[pos].numBin[22];
			instr[pos].indice[2] = instr[pos].numBin[21];
			instr[pos].indice[1] = instr[pos].numBin[20];
			instr[pos].indice[0] = instr[pos].numBin[19];		
		}	
	}
	if(tamanho_cache == 16){
		indice = ((tamanho_cache*1024) / tamanho_bloco);
		if(indice == 512){
			//calcular = 9
			instr[pos].numindice = 9;
			if(instr[pos].numoffset == 5){
				instr[pos].indice[8] = instr[pos].numBin[26];
				instr[pos].indice[7] = instr[pos].numBin[25];
				instr[pos].indice[6] = instr[pos].numBin[24];
				instr[pos].indice[5] = instr[pos].numBin[23];
				instr[pos].indice[4] = instr[pos].numBin[22];
				instr[pos].indice[3] = instr[pos].numBin[21];
				instr[pos].indice[2] = instr[pos].numBin[20];
				instr[pos].indice[1] = instr[pos].numBin[19];
				instr[pos].indice[0] = instr[pos].numBin[18];
			}else{
				instr[pos].indice[8] = instr[pos].numBin[25];
				instr[pos].indice[7] = instr[pos].numBin[24];
				instr[pos].indice[6] = instr[pos].numBin[23];
				instr[pos].indice[5] = instr[pos].numBin[22];
				instr[pos].indice[4] = instr[pos].numBin[21];
				instr[pos].indice[3] = instr[pos].numBin[20];
				instr[pos].indice[2] = instr[pos].numBin[19];	
				instr[pos].indice[1] = instr[pos].numBin[18];
				instr[pos].indice[0] = instr[pos].numBin[17];		
			}	
		}
	}
		if(indice == 256){
			//calcular = 8
			instr[pos].numindice = 9;
			if(instr[pos].numoffset == 5){
				instr[pos].indice[7] = instr[pos].numBin[26];
				instr[pos].indice[6] = instr[pos].numBin[25];
				instr[pos].indice[5] = instr[pos].numBin[24];
				instr[pos].indice[4] = instr[pos].numBin[23];
				instr[pos].indice[3] = instr[pos].numBin[22];
				instr[pos].indice[2] = instr[pos].numBin[21];
				instr[pos].indice[1] = instr[pos].numBin[20];
				instr[pos].indice[0] = instr[pos].numBin[19];

			}else{
				instr[pos].indice[7] = instr[pos].numBin[25];
				instr[pos].indice[6] = instr[pos].numBin[24];
				instr[pos].indice[5] = instr[pos].numBin[23];
				instr[pos].indice[4] = instr[pos].numBin[22];
				instr[pos].indice[3] = instr[pos].numBin[21];
				instr[pos].indice[2] = instr[pos].numBin[20];
				instr[pos].indice[1] = instr[pos].numBin[19];	
				instr[pos].indice[0] = instr[pos].numBin[18];

			}	
		}
	}

}


